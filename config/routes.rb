Rails.application.routes.draw do
  devise_for :freelancers
  devise_for :customers

  root to: 'pages#index'

  get 'pages/index'
  get 'pages/login'
  get 'pages/sign_up'

  resource :freelancer_profile, only: %i[edit update], controller: :freelancer_profile
end
