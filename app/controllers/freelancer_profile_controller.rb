class FreelancerProfileController < ApplicationController
  before_action :set_user

  def edit

  end

  def update
    if @user.update(profile_params)
      redirect_to edit_freelancer_profile_path, notice: 'Profile was successfully updated'
    else
      flash.now[:alert] = 'Please review the problems below:'
      render :edit
    end
  end

  private

  def profile_params
    params.require(:freelancer).permit(:first_name, :last_name, :location, :description)
  end

  def set_user
    @user = current_user
  end

end
