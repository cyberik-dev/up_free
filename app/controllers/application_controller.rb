class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery
  devise_group :user, contains: [:freelancer , :customer]
end
