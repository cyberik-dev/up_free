module ApplicationHelper
  def page_title(text)
    content_for :title, text
  end
end
